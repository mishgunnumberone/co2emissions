//
//  CO2EmissionsController.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 10/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit
import SnapKit
import Charts

class CO2EmissionsController: UIViewController {
    
    //MARK: - UI Elements
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "CO₂ Emissions"
        label.textColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        label.numberOfLines = 1
        label.textAlignment  = .center
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "AvenirNext-DemiBoldItalic", size: 40)
        return label
    }()
    
    lazy var countrySelectContainer: ContentContainer = {
        let container = ContentContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    lazy var countryContainer: CountryContainer = {
       let container = CountryContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(firstCountryContainerTapped(sender:)))
        container.addGestureRecognizer(tap)
        return container
    }()
    
    lazy var comparisonCountryContainer: CountryContainer = {
        let container = CountryContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(comparisonCountryContainerTapped(sender:)))
        container.addGestureRecognizer(tap)
        return container
    }()
    
    lazy var perCapitaSwitchContainer: ContentContainer = {
        let container = ContentContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    lazy var perCapitaLabel: UILabel = {
        var label = UILabel()
        label.text = "Per capita"
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        label.textColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        label.numberOfLines = 1
        label.textAlignment = NSTextAlignment.left
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    lazy var perCapitaSwitch: UISwitch = {
       var perCapitaSwitch = UISwitch()
        return perCapitaSwitch
    }()
    
    lazy var chartContainer: ContentContainer = {
        let container = ContentContainer()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    lazy var countryChart: BarChartView = {
        let chart = BarChartView()
        return chart
    }()
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let colorTop =  UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 98.0/255.0, green: 176.0/255.0, blue: 241.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        initUI()
        drawChart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    
    //MARK: - UI Initializer
    
    func initUI() {
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(36)
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
        }
        
        
        view.addSubview(countrySelectContainer)
        countrySelectContainer.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(30)
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
        }
        
        countrySelectContainer.addSubview(countryContainer)
        countryContainer.snp.makeConstraints { (make) in
            make.top.equalTo(countrySelectContainer.snp.top).offset(10)
            make.left.equalTo(countrySelectContainer.snp.left).offset(8)
            make.right.equalTo(countrySelectContainer.snp.right).offset(-8)
            make.height.equalTo(50)
        }
        countryContainer.applyElementsConstraints()
        
        
        countrySelectContainer.addSubview(comparisonCountryContainer)
        comparisonCountryContainer.snp.makeConstraints { (make) in
            make.top.equalTo(countryContainer.snp.bottom).offset(10)
            make.left.equalTo(countrySelectContainer.snp.left).offset(8)
            make.right.equalTo(countrySelectContainer.snp.right).offset(-8)
            make.bottom.equalTo(countrySelectContainer.snp.bottom).offset(-10)
            make.height.equalTo(50)
        }
        comparisonCountryContainer.applyElementsConstraints()
        comparisonCountryContainer.countryName.text = "Compare to..."
        comparisonCountryContainer.backgroundColor = UIColor.white
        
        
        view.addSubview(perCapitaSwitchContainer)
        perCapitaSwitchContainer.snp.makeConstraints { (make) in
            make.top.equalTo(countrySelectContainer.snp.bottom).offset(10)
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.height.equalTo(45)
        }
        
        perCapitaSwitchContainer.addSubview(perCapitaSwitch)
        perCapitaSwitch.snp.makeConstraints { (make) in
            make.right.equalTo(perCapitaSwitchContainer.snp.right).offset(-10)
            make.centerY.equalTo(perCapitaSwitchContainer.snp.centerY)
        }
        perCapitaSwitch.addTarget(self, action: #selector(self.perCapidaSwitchValueChanged(sender:)), for: .valueChanged)
        
        perCapitaSwitchContainer.addSubview(perCapitaLabel)
        perCapitaLabel.snp.makeConstraints { (make) in
            make.top.equalTo(perCapitaSwitchContainer.snp.top).offset(5)
            make.bottom.equalTo(perCapitaSwitchContainer.snp.bottom).offset(-5)
            make.left.equalTo(perCapitaSwitchContainer.snp.left).offset(8)
            make.right.equalTo(perCapitaSwitch.snp.right).offset(-8)
        }
        
        view.addSubview(chartContainer)
        chartContainer.snp.makeConstraints { (make) in
            make.top.equalTo(perCapitaSwitchContainer.snp.bottom).offset(10)
            make.left.equalTo(view).offset(15)
            make.right.equalTo(view).offset(-15)
            make.bottom.equalTo(view.snp.bottom).offset(-15)
        }
        
        chartContainer.addSubview(countryChart)
        countryChart.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalTo(chartContainer)
        }
    }
    
    // MARK: - Draw chart function
    
    func drawChart() {
        
        if comparisonCountryContainer.countryName.text! == "Compare to..." {
            
            if perCapitaSwitch.isOn {
                let set = getPopulationDataSetForCountry(countryName: countryContainer.countryName.text!)
                let data = BarChartData(dataSet: set)
                countryChart.data = data
                countryChart.setVisibleXRange(minXRange: 7, maxXRange: 7)
                countryChart.scaleXEnabled = false
                countryChart.scaleYEnabled = false
                
            } else {
                let set = getEmissionDataSetForCountry(countryName: countryContainer.countryName.text!)
                let data = BarChartData(dataSet: set)
                countryChart.data = data
                countryChart.setVisibleXRange(minXRange: 7, maxXRange: 7)
                countryChart.scaleXEnabled = false
                countryChart.scaleYEnabled = false
                
            }
            
        } else {
            
            if perCapitaSwitch.isOn {
                let set = getPopulationDataSetForCountry(countryName: countryContainer.countryName.text!)
                let comparionSet = getPopulationDataSetForCountry(countryName: comparisonCountryContainer.countryName.text!)
                comparionSet.setColor(NSUIColor(red: 0.9725, green: 0.7098, blue: 1, alpha: 1.0))
                let data = BarChartData(dataSets: [set, comparionSet])
                
                let groupSpace = 0.11
                let barSpace = 0.07
                let barWidth = 0.375
                
                
                data.barWidth = barWidth
                data.groupBars(fromX: set.xMin-0.5, groupSpace: groupSpace, barSpace: barSpace)
                
                countryChart.data = data
                countryChart.setVisibleXRange(minXRange: 7, maxXRange: 7)
                countryChart.scaleXEnabled = false
                countryChart.scaleYEnabled = false
                
            } else {
                
                let set = getEmissionDataSetForCountry(countryName: countryContainer.countryName.text!)
                let comparionSet = getEmissionDataSetForCountry(countryName: comparisonCountryContainer.countryName.text!)
                comparionSet.setColor(NSUIColor(red: 0.9725, green: 0.7098, blue: 1, alpha: 1.0))
                let data = BarChartData(dataSets: [set, comparionSet])
                
                let groupSpace = 0.11
                let barSpace = 0.07
                let barWidth = 0.375
                
                
                data.barWidth = barWidth
                data.groupBars(fromX: set.xMin-0.5, groupSpace: groupSpace, barSpace: barSpace)
                
                countryChart.data = data
                countryChart.setVisibleXRange(minXRange: 7, maxXRange: 7)
                countryChart.scaleXEnabled = false
                countryChart.scaleYEnabled = false
                
            }
            
        }
    }
    
    //MARK: - Functions for emission/population
    
    func getEmissionDataSetForCountry(countryName name: String) -> BarChartDataSet {
        let (headerRowData, countryData) = EmissionsDataService.shared.getEmissionsForCountry(countryName: name)
        
        var countryChartEntry = [BarChartDataEntry]()
        
        for i in 0..<headerRowData.count {
            if !countryData[i].isEmpty {
                let dvalue = BarChartDataEntry(x: Double(headerRowData[i])!, y: Double(countryData[i])!)
                countryChartEntry.append(dvalue)
            } else {
                let dvalue = BarChartDataEntry(x: Double(headerRowData[i])!, y: 0.0)
                countryChartEntry.append(dvalue)
            }
            
        }
        
        let set = BarChartDataSet(values: countryChartEntry, label: name)
        
        return set
    }
    
    func getPopulationDataSetForCountry(countryName name: String) -> BarChartDataSet {
        let (headerRowData, countryData) = EmissionsDataService.shared.getEmissionsForCountry(countryName: name)
        let countryPopulation = PopulationDataService.shared.getPopulationForCountry(countryName: name)
        
        var barChartEntry = [BarChartDataEntry]()
        
        for i in 0..<headerRowData.count {
            if !countryData[i].isEmpty && !countryPopulation[i].isEmpty{
                let dvalue = BarChartDataEntry(x: Double(headerRowData[i])!, y: Double(countryData[i])!/Double(countryPopulation[i])!)
                barChartEntry.append(dvalue)
            } else {
                let dvalue = BarChartDataEntry(x: Double(headerRowData[i])!, y: 0.0)
                barChartEntry.append(dvalue)
            }
            
        }
        let set = BarChartDataSet(values: barChartEntry, label: name)
        return set
    }
    
    //MARK: - Selectors
    
    @objc func perCapidaSwitchValueChanged(sender: UISwitch) {
        drawChart()
    }
    
    @objc func firstCountryContainerTapped(sender: UIView) {
        let countries = EmissionsDataService.shared.getCountryNames()
        let vc = CountryChoiceController()
        vc.countries = countries
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func comparisonCountryContainerTapped(sender: UIView) {
        var countries = EmissionsDataService.shared.getCountryNames()
        countries.insert("None", at: 0)
        let vc = CountryChoiceController()
        vc.countries = countries
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)    }
}

//MARK: - Delegate for data exchange

extension CO2EmissionsController: CountrySelectedDelegate {
    func countrySelected(value: String, isItCountryToCompare: Bool) {
        if isItCountryToCompare {
            if value == "None" {
                comparisonCountryContainer.countryName.text = "Compare to..."
                comparisonCountryContainer.backgroundColor = UIColor.white
            } else {
                comparisonCountryContainer.countryName.text = value
                comparisonCountryContainer.backgroundColor = UIColor(red: 185.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            }
        } else {
            countryContainer.countryName.text = value
        }
        drawChart()
    }
}


