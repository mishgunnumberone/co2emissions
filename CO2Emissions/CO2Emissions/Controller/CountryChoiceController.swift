//
//  CountryChoiceController.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 11/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit
import SnapKit


class CountryChoiceController: UIViewController {
    func countryWillSelected(values: [String]) {
        countries = values
    }
    
    var delegate: CountrySelectedDelegate?
    
    var countries = [String]()
    
    lazy var countryTable: UITableView = {
       let table = UITableView()
        table.dataSource = self
        table.delegate = self
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Country List"
        
        view.addSubview(countryTable)
        countryTable.snp.makeConstraints { (make) in
            make.top.right.bottom.left.equalTo(view)
        }
    }
}

extension CountryChoiceController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = countries[indexPath.row]
        return cell
    }
    
}

extension CountryChoiceController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.popViewController(animated: true)
        countries.contains("None") ? delegate?.countrySelected(value: countries[indexPath.row], isItCountryToCompare: true) : delegate?.countrySelected(value: countries[indexPath.row], isItCountryToCompare: false)
    }
    
}
