//
//  CountrySelectedDelegate.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 13/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import Foundation

//Delegate for data exchange between CountryChoiceController -> CO2EmissionsController
protocol CountrySelectedDelegate {
    func countrySelected(value: String, isItCountryToCompare: Bool)
}
