//
//  PopulationDataService.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 11/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import Foundation
import  CSV

class PopulationDataService {
    
    public static let shared = PopulationDataService()
    
    func getPopulationForCountry(countryName name: String) -> [String] {
        
        let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "population", ofType: "csv", inDirectory: nil)!)!
        do {
            var countryData = [String]()
            var headerRowData = [String]()
            
            let csv = try CSVReader(stream: stream,
                                    hasHeaderRow: true)
            headerRowData = csv.headerRow!
            headerRowData = Array(headerRowData.dropFirst(4))
            while csv.next() != nil {
                if csv["Country Name"] == name {
                    for i in headerRowData {
                        countryData.append(csv[i]!)
                    }
                }
            }
            return countryData
        } catch _ {
            fatalError()
        }
    }
}
