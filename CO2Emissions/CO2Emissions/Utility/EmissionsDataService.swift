//
//  EmissionsDataService.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 11/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import Foundation
import CSV

class EmissionsDataService {
    
    public static let shared = EmissionsDataService()
    
    func getEmissionsForCountry(countryName name: String) -> ([String], [String]) {
        
        let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "emissions", ofType: "csv", inDirectory: nil)!)!
        do {
            var countryData = [String]()
            var headerRowData = [String]()
            
            let csv = try CSVReader(stream: stream,
                                    hasHeaderRow: true)
            headerRowData = csv.headerRow!
            headerRowData = Array(headerRowData.dropFirst(4))
            headerRowData = Array(headerRowData.dropLast())
            while csv.next() != nil {
                if csv["Country Name"] == name {
                    for i in headerRowData {
                        countryData.append(csv[i]!)
                    }
                }
            }
            return (headerRowData, countryData)
        } catch _ {
            fatalError()
        }
        
    }
    
    func getCountryNames() -> [String] {
        
        let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "emissions", ofType: "csv", inDirectory: nil)!)!
        do {
            var countries = [String]()
            
            let csv = try CSVReader(stream: stream,
                                    hasHeaderRow: true)
            while csv.next() != nil {
                countries.append(csv["Country Name"]!)
            }
            
            return countries
            
        } catch _ {
            fatalError()
        }
        
    }
    
}
