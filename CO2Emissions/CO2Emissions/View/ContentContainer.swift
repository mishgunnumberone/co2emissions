//
//  ContentContainer.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 10/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit

class ContentContainer: UIView {
    
    init() {
        super.init(frame: UIScreen.main.bounds);
        
        self.backgroundColor = UIColor.white
        
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 4.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
