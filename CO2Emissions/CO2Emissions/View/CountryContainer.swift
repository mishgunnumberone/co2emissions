//
//  CountryContainer.swift
//  CO2Emissions
//
//  Created by Mikhail Bobretsov on 10/01/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit
import SnapKit

class CountryContainer: UIView {

    lazy var rightArrowIcon: UIImageView = {
        var image = UIImageView(image: UIImage.init(named: "iOS Arrow Right"))
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var countryName: UILabel = {
        var label = UILabel()
        label.text = "Finland"
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        label.textColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        label.numberOfLines = 1
        label.textAlignment = NSTextAlignment.left
        label.minimumScaleFactor = 0.8
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: UIScreen.main.bounds);

        self.backgroundColor = UIColor(red: 185.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 185.0/255.0, green: 246.0/255.0, blue: 202.0/255.0, alpha: 1.0).cgColor
        
        
        

        }
    
    func applyElementsConstraints() {
        self.addSubview(rightArrowIcon)
        rightArrowIcon.snp.makeConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-8)
            make.top.equalTo(self.snp.top).offset(4)
            make.bottom.equalTo(self.snp.bottom).offset(-4)
            make.width.equalTo(self.frame.width * 0.1)
        }
        
        self.addSubview(countryName)
        countryName.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(13)
            make.top.equalTo(self.snp.top).offset(4)
            make.right.equalTo(rightArrowIcon.snp.left).offset(-8)
            make.bottom.equalTo(self.snp.bottom).offset(-4)

        }
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
